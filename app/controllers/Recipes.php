<?php
/**
 * Created by PhpStorm.
 * User: leonardo
 * Date: 15.06.18
 * Time: 16:18
 */

class Recipes extends Controller
{
    /**
     * @var RecipeModel
     */
    private $recipeModel;
    /**
     * @var UserModel
     */
    private $userModel;

    public function __construct()
    {
        if (!isLoggedIn()) {
            redirect('users/login');
        }

        $this->recipeModel = $this->model('RecipeModel');
        $this->userModel = $this->model('UserModel');
    }

    public function index()
    {
        $data = [
            'recipes' => $this->recipeModel->getRecipes(),
        ];
        $this->render('recipes/index', $data);
    }

    public function add()
    {
        // Check for POST
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Sanitize POST data
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [
                'name' => trim($_POST['name']),
                'content' => trim($_POST['content']),
                'ingridients' => trim($_POST['ingridients']),
                'user_id' => $_SESSION['user_id'],
                'name_err' => '',
                'content_err' => '',
                'ingridients_err' => '',
            ];


            // Validate Name
            if (empty($data['name'])) {
                $data['name_err'] = 'Please enter the recipes name';
            }

            // Validate Content
            if (empty($data['content'])) {
                $data['content_err'] = 'Please enter the instructions';
            }
            // Validate Ingridients
            if (empty($data['ingridients'])) {
                $data['ingridients_err'] = 'Please enter the ingridients';
            }

            // Make sure errors are empty
            if (empty($data['content_err']) && empty($data['name_err']) && empty($data['ingridients_err'])) {
                // Validated
                if ($this->recipeModel->addRecipe($data)) {
                    handleFlash('recipe_message', 'Recipe Added');
                    redirect('recipes/index');
                } else {
                    die('something went wrong');
                }
            } else {
                $this->render('recipes/add', $data);
            }

        } else {
            $data = [
                'name' => '',
                'content' => '',
                'ingridients' => '',
            ];
            $this->render('recipes/add', $data);
        }
    }

    public function edit($id)
    {
        // Check for POST
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Sanitize POST data
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [
                'id' => $id,
                'name' => trim($_POST['name']),
                'content' => trim($_POST['content']),
                'ingridients' => trim($_POST['ingridients']),
                'user_id' => $_SESSION['user_id'],
                'name_err' => '',
                'content_err' => '',
                'ingridients_err' => '',
            ];

            // Validate Name
            if (empty($data['name'])) {
                $data['name_err'] = 'Please enter the recipes name';
            }

            // Validate Content
            if (empty($data['content'])) {
                $data['content_err'] = 'Please enter the instructions';
            }
            // Validate Ingridients
            if (empty($data['ingridients'])) {
                $data['ingridients_err'] = 'Please enter the ingridients';
            }

            // Make sure errors are empty
            if (empty($data['content_err']) && empty($data['name_err']) && empty($data['ingridients_err'])) {
                // Validated
                if ($this->recipeModel->updateRecipe($data)) {
                    handleFlash('recipe_message', 'Recipe Updated');
                    redirect('recipes/index');
                } else {
                    die('something went wrong');
                }
            } else {
                $this->render('recipes/edit', $data);
            }

        } else {
            // Get existing post from modeö
            $recipe = $this->recipeModel->getRecipeById($id);

            if ($recipe->user_id != $_SESSION['user_id']) {
                redirect('recipes/index');
            }

            $data = [
                'id' => $id,
                'name' => $recipe->name,
                'content' => $recipe->content,
                'ingridients' => $recipe->ingridients,
            ];
            $this->render('recipes/edit', $data);
        }
    }

    public function show($id)
    {
        $recipe = $this->recipeModel->getRecipeById($id);
        $user = $this->userModel->getUserById($recipe->user_id);
        $data = [
            'recipe' => $recipe,
            'user' => $user,
        ];
        $this->render('recipes/show', $data);
    }

    public function delete($id)
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Get existing recipe
            $recipe = $this->recipeModel->getRecipeById($id);

            // Check owner
            if ($recipe->user_id != $_SESSION['user_id']) {
                redirect('recipes/index');
            }
            if ($this->recipeModel->deleteRecipe($id)) {
                handleFlash('recipe_message', 'Recipe Removed');
                redirect('recipes/index');
            } else {
                die('something went wrong');
            }
        } else {
            redirect('recipes/index');
        }

    }
}