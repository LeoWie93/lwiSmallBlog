<?php

class Posts extends Controller
{
    public function __construct()
    {
    }

    public function index()
    {
        if (isLoggedIn()) {
            redirect('recipes');
        }

        $data =
            [
                'title' => 'SmallBlog',
                'description' => 'Simple blog built on the lwiPHPFramework',
            ];
        $this->render('posts/index', $data);
    }

    public function about()
    {
        $data =
            [
                'title' => 'About us',
                'description' => 'App to create and manage Posts',
            ];
        $this->render('posts/about', $data);
    }
}