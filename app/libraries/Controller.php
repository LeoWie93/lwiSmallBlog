<?php
/**
 * Base Controller
 * Loads the models and views
 */

class Controller
{
    /**
     * @param $model
     * @return mixed
     */
    public function model($model)
    {
        // Require model file
        require_once '../app/models/' . $model . '.php';

        // Instantiate model
        return new $model;
    }

    /**
     * @param $view
     * @param array $data
     */
    public function render($view, array $data = [])
    {
        // Check for view file
        if (file_exists('../app/views/' . $view . '.php')) {
            require_once '../app/views/' . $view . '.php';
        } else {
            // TODO handle view not found exception
            // View does not exist
            die('View does not exist');
        }
    }
}