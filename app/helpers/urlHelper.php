<?php

// Simple page redirect
function redirect(string $page)
{
    header('location:' . URLROOT . '/' . $page);
}
