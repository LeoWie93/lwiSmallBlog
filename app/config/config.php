<?php
// DB Params
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', 'toor');
define('DB_NAME', 'smallBlog');

// App Root
define('APPROOT', dirname(__DIR__));
// URL Root
define('URLROOT', 'http://localhost/lwiSmallBlog');
// Site Name
define('SITENAME', 'SmallBlog');
// App Version
define('APPVERSION', '1.0.0');
