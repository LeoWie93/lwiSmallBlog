<?php require APPROOT . '/views/_parts/header.php'; ?>
<a href="<?php echo URLROOT; ?>/recipes" class="btn btn-light"><i class="fa fa-backward"></i>Back</a>
<br>
<h1><?php echo $data['recipe']->name ?></h1>
<div class="bg-secondary text-white p-2 mb-3">
    Written by <?php echo $data['user']->name; ?> on <?php echo $data['recipe']->created_at; ?>
</div>
<p><?php echo $data['recipe']->content; ?></p>

<?php if ($data['recipe']->user_id == $_SESSION['user_id']): ?>
    <hr>
    <a href="<?php echo URLROOT; ?>/recipes/edit/<?php echo $data['recipe']->id; ?>" class="btn btn-dark">Edit</a>
    <form class="pull-right" action="<?php echo URLROOT; ?>/recipes/delete/<?php echo $data['recipe']->id ?>" method="post">
        <input type="submit" value="Delete" class="btn btn-danger">
    </form>
<?php endif; ?>
<?php require APPROOT . '/views/_parts/footer.php'; ?>
