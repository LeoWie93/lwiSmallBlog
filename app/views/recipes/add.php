<?php require APPROOT . '/views/_parts/header.php'; ?>
<a href="<?php echo URLROOT; ?>/recipes" class="btn btn-light"><i class="fa fa-backward"></i>Back</a>
<div class="card card-body bg-light mt-5">
    <?php handleFlash('register_success') ?>
    <h2>
        Add Recipe
    </h2>
    <p>Create a new recipe with this form</p>
    <form action="<?php echo URLROOT; ?>/recipes/add"
          method="post">
        <div class="form-group">
            <label for="name">Rezeptname: <sub>*</sub></label>
            <input type="text"
                   name="name"
                   class="form-control form-control-lg
                               <?php echo (!empty($data['name_err'])) ? ' is-invalid' : ''; ?>"
                   value="<?php echo $data['name']; ?>">
            <span class="invalid-feedback"><?php echo $data['name_err']; ?></span>
        </div>
        <div class="form-group">
            <label for="content">Content: <sub>*</sub></label>
            <textarea name="content"
                      class="form-control form-control-lg
                            <?php echo (!empty($data['content_err'])) ? ' is-invalid' : ''; ?>"
            ><?php echo $data['content']; ?></textarea>
            <span class="invalid-feedback"><?php echo $data['content_err']; ?></span>
        </div>
        <div class="form-group">
            <label for="ingridients">Ingridients: <sub>*</sub></label>
            <textarea name="ingridients"
                      class="form-control form-control-lg
                               <?php echo (!empty($data['ingridients_err'])) ? ' is-invalid' : ''; ?>"
            ><?php echo $data['ingridients']; ?></textarea>
            <span class="invalid-feedback"><?php echo $data['ingridients_err']; ?></span>
        </div>
        <input type="submit" class="btn btn-success" value="Submit">
    </form>
</div>
<?php require APPROOT . '/views/_parts/footer.php'; ?>
