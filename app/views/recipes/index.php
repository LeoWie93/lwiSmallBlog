<?php require APPROOT . '/views/_parts/header.php'; ?>
<?php handleFlash('recipe_message')?>
<div class="row mb-3">
    <div class="col-md-6">
        <h1>Recipes</h1>
    </div>
    <div class="col-md-6">
        <a href="<?php echo URLROOT; ?>/recipes/add" class="btn btn-primary pull-right">
            <i class="fa fa-pencil"></i> Add Recipe
        </a>
    </div>
</div>
<?php foreach ($data['recipes'] as $recipe) : ?>
    <div class="card card-body mb-3">
        <h4 class="card-title"><?php echo $recipe->recipeName; ?></h4>
        <div class="bg-light p-2 mb-3">
            written by <?php echo $recipe->userName; ?> on <?php echo $recipe->recipeCreated; ?>
        </div>
        <p class="card-text"><?php echo $recipe->content; ?></p>
        <p class="card-text"><?php echo $recipe->ingridients; ?></p>
        <a href="<?php echo URLROOT; ?>/recipes/show/<?php echo $recipe->recipeId; ?>" class="btn btn-dark">More</a>
    </div>
<?php endforeach; ?>
<?php require APPROOT . '/views/_parts/footer.php'; ?>
