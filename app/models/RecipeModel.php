<?php

class RecipeModel
{
    /**
     * @var Database
     */
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function getRecipes(): array
    {
        $this->db->query('SELECT *,
                              recipes.id as recipeId,
                              users.id as userId,
                              recipes.name as recipeName,
                              users.name as userName,
                              recipes.created_at as recipeCreated,
                              users.created_at as userCreated
                              FROM recipes
                              INNER JOIN users
                              ON recipes.user_id = users.id
                              ORDER BY recipes.created_at DESC
                              ');

        $results = $this->db->resultSet();

        return $results;
    }

    public function getRecipeById(int $id)
    {
        $this->db->query('SELECT * FROM recipes WHERE id = :id');
        $this->db->bind(':id', $id);

        $row = $this->db->single();

        return $row;
    }

    public function addRecipe(array $data)
    {
        $this->db->query('INSERT INTO recipes(name,user_id,content,ingridients)
                              VALUES (:name,:user_id,:content,:ingridients)');
        $this->db->bind(':name', $data['name']);
        $this->db->bind(':user_id', $data['user_id']);
        $this->db->bind(':content', $data['content']);
        $this->db->bind(':ingridients', $data['ingridients']);

        return $this->db->execute();
    }

    public function updateRecipe(array $data)
    {
        $this->db->query('UPDATE recipes SET name = :name,content = :content,ingridients = :ingridients WHERE id = :id');
        $this->db->bind(':id', $data['id']);
        $this->db->bind(':name', $data['name']);
        $this->db->bind(':content', $data['content']);
        $this->db->bind(':ingridients', $data['ingridients']);

        return $this->db->execute();
    }

    public function deleteRecipe(int $id)
    {
        $this->db->query('DELETE FROM recipes WHERE id = :id');
        $this->db->bind(':id', $id);

        return $this->db->execute();
    }
}