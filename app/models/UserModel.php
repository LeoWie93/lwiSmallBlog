<?php

class UserModel
{
    /**
     * @var Database
     */
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    /**
     * @param string $email
     * @return bool
     */
    public function findUserByEmail(string $email): bool
    {
        $this->db->query('SELECT * FROM users WHERE email = :email');
        $this->db->bind(':email', $email);

        $row = $this->db->single();

        // CHeck row
        if ($this->db->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getUserById(int $id)
    {
        $this->db->query('SELECT * FROM users WHERE id = :id');
        $this->db->bind(':id', $id);

        $row = $this->db->single();

        return $row;
    }

    // Register new user
    public function newUser(array $data): bool
    {
        $this->db->query('INSERT INTO users(name,email,password) VALUES (:name,:email,:password)');
        $this->db->bind(':name', $data['name']);
        $this->db->bind(':email', $data['email']);
        $this->db->bind(':password', $data['password']);

        return $this->db->execute();
    }

    // Login User
    public function login(string $email, string $unhashedPassword)
    {
        $this->db->query('SELECT * FROM users WHERE email = :email');
        $this->db->bind(':email', $email);

        $row = $this->db->single();

        $hasedPassword = $row->password;
        if (!password_verify($unhashedPassword, $hasedPassword)) {
            return false;
        }
        return $row;
    }
}